from zephir.i18n import _
from .query import (insert_site, fetch_site, describe_site, erase_site)


class Site:

    def create_site(self, cursor,
                    sitename,
                    sitedescription,
                    parentsiteid):
        return insert_site(cursor, sitename, sitedescription, parentsiteid)[0]

    def describe_site(self, cursor, siteid):
        return describe_site(cursor, siteid)

    def list_site(self, cursor):
        return fetch_site(cursor)

    def erase_site(self, cursor):
        erase_site(cursor)
