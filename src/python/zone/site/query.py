from zephir.i18n import _
from .error import SiteError


"""insert site
"""
SITE_INSERT = """
INSERT INTO site (sitename, sitedescription, siteparentid)
VALUES (%s, %s, %s)
RETURNING siteid
"""


"""select all sites
"""
SITE_SELECT_ALL = """
SELECT siteid, sitename, sitedescription, siteparentid FROM site
"""


"""select only one site
"""
SITE_SELECT_ONE = """
SELECT siteid, sitename, sitedescription, siteparentid FROM site
WHERE siteid=%s
"""

#"""
#"""
#SITE_UPDATE = """
#"""
#
#"""
#"""
#SITE_DELETE = """
#"""

"""erase all data in site table
"""
ERASE_SITE = """
DELETE FROM site
"""

def insert_site(cursor, sitename: str, sitedescription: str, siteparentid: int):
    cursor.execute(SITE_INSERT, (sitename, sitedescription, siteparentid))
    return cursor.fetchone()


def fetch_site(cursor):
    cursor.execute(SITE_SELECT_ALL)
    return cursor.fetchall()


def describe_site(cursor, siteid: int):
    cursor.execute(SITE_SELECT_ONE, (siteid, ))
    result = cursor.fetchone()
    if result is None:
        raise SiteError(_("unable to find site by id {}").format(siteid))
    return result


def erase_site(cursor):
    cursor.execute(ERASE_SITE)
