from zephir.config import ServiceConfig
from zephir.database import connect
from zone.site import Site


def setup_module(module):
    CONF = ServiceConfig()
    CONN = connect(CONF)
    module.CONN = CONN


def teardown_module(module):
    module.CONN.rollback()
    module.CONN.close()


def setup_function(function):
    site = Site()
    with CONN.cursor() as cursor:
        site.erase_site(cursor)


def test_create_site():
    site = Site()
    with CONN.cursor() as cursor:
        siteid1 = site.create_site(cursor, 'nom du site 1', 'description du site 1', None)
        siteid2 = site.create_site(cursor, 'nom du site 2', 'description du site 2', None)
    assert siteid2 - siteid1 == 1


def test_list_site():
    site = Site()
    sites = range(1,11)
    with CONN.cursor() as cursor:
        siteids = [site.create_site(cursor, 'nom du site {}'.format(i),
                                            'description du site {}'.format(i),
                                            None)
                   for i in sites]
        site_list = site.list_site(cursor)
    assert site_list == [(serial, 'nom du site {}'.format(i), 'description du site {}'.format(i), None) for serial, i in zip(siteids, sites)]


def test_describe_site():
    site = Site()
    with CONN.cursor() as cursor:
        siteid = site.create_site(cursor, 'nom du site', 'description du site', None)
        site_description = site.describe_site(cursor, siteid)
    assert site_description == (siteid, 'nom du site', 'description du site', None)
