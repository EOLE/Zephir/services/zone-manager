-- Deploy servermodel:servermodel_schema to pg

BEGIN;

-- Machine table creation
CREATE TABLE Machine (
  MachineId SERIAL PRIMARY KEY,
  MachineName VARCHAR(255) NOT NULL,
  MachineServers INTEGER
);

-- Site table creation
CREATE TABLE Site (
  SiteId SERIAL PRIMARY KEY,
  SiteName VARCHAR(255) NOT NULL,
  SiteDescription VARCHAR(255) NOT NULL,
  SiteParentId INTEGER,
  FOREIGN KEY (SiteParentId) REFERENCES Site(SiteId)
);

-- Zone table creation
CREATE TABLE Zone (
  ZoneId SERIAL PRIMARY KEY,
  ZoneName VARCHAR(255) NOT NULL,
  ZoneCIDR VARCHAR(255) NOT NULL,
  SiteId INTEGER,
  FOREIGN KEY (SiteId) REFERENCES Site(SiteId)
);

COMMIT;
